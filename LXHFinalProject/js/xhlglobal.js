/**
 * File Name: xhlglobal.js
 *
 * Revision History:
 *       Xinghua Li, 2019-11-01 : Created
 */

$(document).ready(function () {
    init();
    initDB();
});

function init() {
    $("#xhlAddSave").on("click", xhlAddSave_click);
    $("#xhlModifyAddUpdate1").on("click", xhlDoValidation2);
    $("#xhlModifyAddUpdate2").on("click", xhlDoValidation3);
}

function xhlAddSave_click() {
    xhlAddFeedback();
    alert("New Feedback Added!\n \t Prevent this page from creating additional dialogs");
}

function xhlDoValidation2() {
    testValidation2();
}

function xhlDoValidation3() {
    testValidation3();
}

//Hide or Show food rate area
function chkShowOrHide(id) {
    let div=document.getElementById(id);
    if(div.style.display=='block')
        div.style.display='none';
    else
        div.style.display='block';
}

function chkShowOrHideM1() {
    let chkFoodRate=document.getElementById("xhlFoodRateForModify1");
    if(chkFoodRate.checked=true){
        chkFoodRate.style.display="block";
    }
    else {
        chkFoodRate.style.display="none";
    }
}

function chkShowOrHideM2() {
    let chkFoodRate=document.getElementById("xhlFoodRateForModify2");
    if(chkFoodRate.checked=true){
        chkFoodRate.style.display="block";
    }
    else {
        chkFoodRate.style.display="none";
    }
}

//save default email
function saveEmail() {

    localStorage.setItem("DefaultEmail", "xli7782@conestogac.on.ca");
    alert("Default reviewer email saved \n \t Prevent this page from creating additional dialogs");
}

function getEmail(dEmail) {
    var e=document.getElementById(dEmail);
    var dfEmail
    dfEmail=localStorage.getItem("DefaultEmail");
    e.placeholder=dfEmail;
}

//calculate food rate and display
//for add page
function calculateRate(){
    var xhlQuality=parseInt($("#xhlQuality").val());
    var xhlService=parseInt($("#xhlService").val());
    var xhlValue=parseInt($("#xhlValue").val());
    var overRate=((xhlQuality+xhlService+xhlValue)*100/15)+"%";
    $("#xhlOverRate").val(overRate);
}

//for modify page1
function calculateRateM1(){
    var xhlQualityM1=parseInt($("#xhlQualityM1").val());
    var xhlServiceM1=parseInt($("#xhlServiceM1").val());
    var xhlValueM1=parseInt($("#xhlValueM1").val());
    var overRate=((xhlQualityM1+xhlServiceM1+xhlValueM1)*100/15)+"%";
    $("#xhlOverRateM1").val(overRate);
}

////for modify page2
function calculateRateM2(){
    var xhlQualityM2=parseInt($("#xhlQualityM2").val());
    var xhlServiceM2=parseInt($("#xhlServiceM2").val());
    var xhlValueM2=parseInt($("#xhlValueM2").val());
    var overRate=((xhlQualityM2+xhlServiceM2+xhlValueM2)*100/15)+"%";
    $("#xhlOverRateM2").val(overRate);
}

//initial FeedbackDB
function initDB(){
    try{
        DB.xhlCreateDatabase();
        if (db) {
            console.info("drop Type table...");
            DB.lxhDropType();
            console.info("Creating Tables...");
            DB.xhlCreateTables();
            //insert 3 records for type table
            xhlAddTypeRecords();
        }
        else{
            console.error("Error: Cannot create tables: Database does not exist!");
        }
    } catch(e){
        console.error("Error: (Fatal) Error in initDB(). Can not proceed.");
    }
}


