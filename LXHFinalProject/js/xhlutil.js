/**
 * File Name: xhlutil.js
 *
 *       Xinghua Li, 2019-11-01 : Created
 */
function testValidation1() {
    if (doValidation1()) {
        console.info("Validation successful");
    }
    else {
        console.error("Validation failed");


    }
}

function testValidation2() {
    if (doValidationM1()) {
        console.info("Validation successful");
    }
    else {
        console.error("Validation failed");


    }
}

function testValidation3() {
    if (doValidationM2()) {
        console.info("Validation successful");
    }
    else {
        console.error("Validation failed");


    }
}

function doValidation1() {
    let form=$("#xhlAddForm");
    form.validate({
        rules: {
            xhlNameAdd: {
                required: true,
                rangelength: [2,20]
            },
            xhlEmail: {
                required: true,
                emailCheck:true
            },
            txtDOBAdd: {
                required: true
            },
            xhlCheckBox: {},
            xhlQuality: {
                required:true,
                range:[0,5]
            },
            xhlService: {
                required:true,
                range:[0,5]
            },
            xhlValue: {
                required:true,
                range:[0,5]
            }
        },
        messages: {
            xhlNameAdd: {
                required:"You must specified your name",
                rangelength: "Name must be at least 2-30 characters long"
            },
            xhlEmail: {
                required: "Please enter a valid email address",
                emailCheck: "Not valid email enter!"
            },
            txtDOBAdd: {
                required: "Review date is required"
            },
            xhlCheckBox: {},
            xhlQuality: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            },
            xhlService: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            },
            xhlValue: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            }
        }

    });
    return form.valid();
}

function doValidationM1() {
    let form1=$("#xhlAddModifyForm1");
    form1.validate({
        rules: {
            xhlNameModify1: {
                required: true,
                rangelength: [2,20]
            },
            xhlEmailM1: {
                required: true,
                emailCheck:true
            },
            xhlReviewDateM1: {
                required: true
            },
            xhlCheckBoxM1: {},
            xhlQualityM1: {
                required:true,
                range:[0,5]
            },
            xhlServiceM1: {
                required:true,
                range:[0,5]
            },
            xhlValueM1: {
                required:true,
                range:[0,5]
            }
        },
        messages: {
            xhlNameModify1: {
                required:"You must specified your name",
                rangelength: "Name must be at least 2-30 characters long"
            },
            xhlEmailM1: {
                required: "Please enter a valid email address",
                emailCheck: "Not valid email enter!"
            },
            xhlReviewDateM1: {
                required: "Review date is required"
            },
            xhlCheckBoxM1: {},
            xhlQualityM1: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            },
            xhlServiceM1: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            },
            xhlValueM1: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            }
        }

    });
    return form1.valid();
}

function doValidationM2() {
    let form2=$("#xhlAddModifyForm2");
    form2.validate({
        rules: {
            xhlNameModify2: {
                required: true,
                rangelength: [2,20]
            },
            xhlEmailM2: {
                required: true,
                emailCheck:true
            },
            xhlReviewDateM2: {
                required: true
            },
            xhlCheckBoxM2: {},
            xhlQualityM2: {
                required:true,
                range:[0,5]
            },
            xhlServiceM2: {
                required:true,
                range:[0,5]
            },
            xhlValueM2: {
                required:true,
                range:[0,5]
            }
        },
        messages: {
            xhlNameModify2: {
                required:"You must specified your name",
                rangelength: "Name must be at least 2-30 characters long"
            },
            xhlEmailM2: {
                required: "Please enter a valid email address",
                emailCheck: "Not valid email enter!"
            },
            xhlReviewDateM2: {
                required: "Review date is required"
            },
            xhlCheckBoxM2: {},
            xhlQualityM2: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            },
            xhlServiceM2: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            },
            xhlValueM2: {
                required:"Please enter a number",
                range: "Value must be 0-5"
            }
        }

    });
    return form2.valid();
}

jQuery.validator.addMethod("emailCheck",
    function(value, element){
        //var regex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var regex=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return this.optional(element) || regex.test(value);
    },
    "Not valid email enter!");




