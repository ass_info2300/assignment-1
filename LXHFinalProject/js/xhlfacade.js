/**
 * File Name: xhlfacade.js
 *
 * Revision History:
 *       Xinghua Li, 2019-11-15 : Created
 */

function showCalculatedAge() {
    var dob = $("#txtDOBAdd").val();

    $("#txtAgeAdd").val(getCurrentAge(dob));
}

function xhlClearDatabase() {
    try{
        var result=confirm("Really want to clear database? \n \t Prevent this page from creating additional dialogs");
        if(result==true){
            DB.lxhDropTables();
            alert("Database Cleared \n \t Prevent this page from creating additional dialogs");
        }
        else {

        }

    } catch(e){
        console.error("Error: (Fatal) Error in initDB(). Can not proceed.");
    }

}

function xhlAddFeedback() {
    try{
        //do validation
        if (doValidation1()) {
            //fetch info from inputs
            var businessName = $("#xhlNameAdd").val();
            var s=document.getElementById("xhlSelect");
            var typeId =s.options[s.selectedIndex].value;
            var reviewerEmail = $("#xhlEmail").val();
            var reviewerComments = $("#xhlComments").val();
            var reviewDate=$("#txtDOBAdd").val();
            var checkRate=document.getElementById("xhlCheckBox");
            if(checkRate.checked==true){        var hasRating = true;}
            else {var hasRating = false;}

            var rating1 = $("#xhlQuality").val();
            var rating2 = $("#xhlService").val();
            var rating3 = $("#xhlValue").val();

            //call DAL function to insert the
            if(hasRating==true)
                var options = [businessName,typeId,reviewerEmail,reviewerComments,reviewDate,hasRating,rating1,rating2,rating3];
            else
                var  options = [businessName,typeId,reviewerEmail,reviewerComments,reviewDate,hasRating,null,null,null];

            function callback() {
                console.info("Success: Record inserted successfully");
            }

            review.insert(options, callback);
        }
        else {
            console.error("Form is not valid");
        }

    } catch(e){
        console.error("Error: (Fatal) Error in initDB(). Can not proceed.");
    }

}
function xhlAddTypeRecords() {
    try{


            function callback() {
                console.info("Success: Type Record inserted successfully");
            }
                var options=["Other"];
                type.insert(options, callback);
                var options=["Asin"];
                type.insert(options, callback);
                var options=["Canadian"];
                type.insert(options, callback);


    } catch(e){
        console.error("Error: (Fatal) Error in initDB(). Can not proceed.");
    }

}
function xhlGetReviews() {
    var options = [];

    function callback(tx, results) {

        var htmlCode = "";

        for (var i = 0; i < results.rows.length; i++) {

            // both will work
            // var row = results.rows.item(i);
            var row = results.rows[i];

            console.info("Id: " + row['id'] +
                " Name: " + row['name'] +
                " Full Name: " + row['fullName'] +
                " DOB: " + row['dob'] +
                " Is Friend: " + row['isFriend']);

            var overRating=calculateRateM1();

            htmlCode += "<li><a data-theme='a' " + " href='#xhlEditFeedbackPage1'>" +
                "<h1>Business Name: " + row['businessName'] + "</h1>" +
                "<h2>Reviewer Email: " + row['reviewerEmail'] + "</h2>" +
                "<h3>Comments: " + row['reviewerComments'] + "</h3>" +
                "<h3>Overall Rating: " + overRating + "</h3>" +
                "</a></li>";
        }

        var lv = $("#xhlFeedbackList");

        lv = lv.html(htmlCode);
        lv.listview("refresh"); // very important
        //attach event handler for each list items
        $("#lvAll a").on("click", clickHandler);

        function clickHandler() {
            localStorage.setItem("id", $(this).attr("data-row-id"));

            //navigate to the detail page automatically
            //both will work.
            $(location).prop('href', '#pageDetail');
            // $.mobile.changePage("#pageDetail", {transition: 'none'});
        }

    }
    review.selectAll(options, callback);
}

function testValidation() {
    if (doValidate_frmExtra()) {
        console.info("Validation successful");
    }
    else {
        console.error("Validation failed");


    }
}

function clearDatabase() {
    var result = confirm("Really want to clear database?");
    if (result) {
        try {
            DB.dropTables();
            alert("Database cleared!");
        } catch (e) {
            alert(e);
        }
    }
}

function updateFriendEnemy() {
    // var id = $("#txtId").val();
    var id = localStorage.getItem("id");

    var name = $("#txtNameModify").val();
    var fullName = $("#txtFullNameModify").val();
    var isFriend = $("#radFriendModify").prop("checked");
    var dob = $("#txtDOBModify").val();

    //very important
    var options = [name, fullName, dob, isFriend, id];

    function callback() {
        console.info("Success: Record updated successfully");
    }

    Friend.update(options, callback);
}

function deleteFriendEnemy() {
    // var id = $("#txtId").val();
    var id = localStorage.getItem("id");
    var options = [id];

    function callback() {
        console.info("Success: Record deleted successfully");
        $(location).prop('href', '#pageFriends');

    }

    Friend.delete(options, callback);
}


function showOneFriendEnemy() {


    // var id = $("#txtId").val();
    var id = localStorage.getItem("id");


    var options = [id];

    function callback(tx, results) {
        var row = results.rows[0];

        console.info("Id: " + row['id'] +
            " Name: " + row['name'] +
            " Full Name: " + row['fullName'] +
            " DOB: " + row['dob'] +
            " Is Friend: " + row['isFriend']);


        $("#txtNameModify").val(row['name']);
        $("#txtFullNameModify").val(row['fullName']);
        if (row['isFriend'] == 'true') {
            $("#radFriendModify").prop("checked", true);
        }
        else {
            $("#radEnemyModify").prop("checked", true);
        }

        $("#frmModifyFriendEnemy :radio").checkboxradio("refresh");

        $("#txtDOBModify").val(row['dob']);

    }

    review.select(options, callback);
}






