/**
 * File Name: xhlglobal.js
 *
 * Revision History:
 *       Xinghua Li, 2019-11-08 : Created
 */

var db;

function errorHandler(tx, error){
    console.error("SQL error: " + tx + " (" + error.code + ") : " + error.message);
}

var DB = {
    xhlCreateDatabase: function(){
        var shortName= "xhlFeedbackDB";
        var version = "1.0";
        var displayName = "DB for xhlFeedbackDB app";
        var dbSize = 2 * 1024 * 1024;

        console.info("Creating Database ...");
        db = openDatabase(shortName, version, displayName, dbSize, dbCreateSuccess);

        function dbCreateSuccess(){
            console.info("Success: Database created successfully.");
        }
    },
    xhlCreateTables: function(){

        function txFunction(tx) {
            console.info("Creating table: type");
            var sql = "CREATE TABLE IF NOT EXISTS type(" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR(20) NOT NULL);";

            console.info("Creating table: review");
            var sql1 = "CREATE TABLE IF NOT EXISTS review(" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "businessName VARCHAR(30) NOT NULL," +
                "typeId INTEGER NOT NULL," +
                "reviewerEmail VARCHAR(30)," +
                "reviewerComments TEXT," +
                "reviewDate DATE," +
                "hasRating VARCHAR(1)," +
                "rating1 INTEGER," +
                "rating2 INTEGER," +
                "rating3 INTEGER," +
                "FOREIGN KEY(typeId) REFERENCES type(id));";

            var options = [];

            function successCreate(){
                console.info("Success: Create table: review successful.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
            tx.executeSql(sql1, options, successCreate, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Create tables transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction );
    },

    lxhDropTables: function(){

        function txFunction(tx){
            var sql = "DROP TABLE IF EXISTS review;";
            var sql1 = "DROP TABLE IF EXISTS type;";
            var options = [];

            function successDrop() {
                console.info("Success: review table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler );
            tx.executeSql(sql1, options, successDrop, errorHandler );
        }

        function successTransaction(){
            console.info("Success: Drop review and type tables transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },

    lxhDropType: function(){
        function txFunction(tx){
            var sql = "DROP TABLE IF EXISTS type;";
            var options = [];

            function successDrop() {
                console.info("Success: type table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler );
        }

        function successTransaction(){
            console.info("Success: Drop type tables transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }

};